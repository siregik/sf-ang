const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpack = require('webpack');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

  module.exports = {
    entry: {
      app: ['./src/app']
      // app: './src/app/index.js'
    },
    resolve: {
      extensions: ['.js', '.scss', '.css', '.svg'],
      modules: [
        path.join(__dirname, '/src/app'),
        path.join(__dirname, '/src/assets'),
        'node_modules',
      ],
      alias: {
        Components: path.resolve(__dirname, 'src/ui/components/'),
      },
    },
    devtool: 'inline-source-map',
    devServer: {
      contentBase: './dist',
      hot: true
    },
    plugins: [
      new CleanWebpackPlugin(['dist']),
      new ExtractTextPlugin({
        filename: 'styles/[name].[hash].css',
      }),
      new HtmlWebpackPlugin({
        template: './src/public/index.html',
        inject: 'body',
        chunksSortMode: 'dependency',
      }),
      new webpack.NamedModulesPlugin(),
      new webpack.HotModuleReplacementPlugin(),
      // new UglifyJSPlugin({
      //   sourceMap: true,
      //   // compress: {
      //   //   warnings: false,
      //   //   screw_ie8: true,
      //   //   conditionals: true,
      //   //   unused: true,
      //   //   comparisons: true,
      //   //   sequences: true,
      //   //   dead_code: true,
      //   //   evaluate: true,
      //   //   if_return: true,
      //   //   join_vars: true,
      //   // },
      //   // output: {
      //   //   comments: false
      //   // }
      // }),
    ],
    output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, 'dist')
    },
   module: {
     rules: [
        {
          // JS LOADER
          // Reference: https://github.com/babel/babel-loader
          // Transpile .js files using babel-loader
          // Compiles ES6 and ES7 into ES5 code
          test: /\.js$/,
          loader: 'babel-loader',
          exclude: /node_modules/,
        },
        {
          // HTML LOADER
          // Reference: https://github.com/webpack-contrib/html-loader
          // Allow loading html through js
          test: /\.html$/,
          loader: 'html-loader',

        },
       {
         test: /\.(scss|css)$/,
         use: [{
            loader: "style-loader" // creates style nodes from JS strings
          }, {
            loader: "css-loader" // translates CSS into CommonJS
          }, {
            loader: "sass-loader" // compiles Sass to CSS
          }]
       },
       {
          test: /\.(png|svg|jpg|gif|woff|woff2|eot|ttf|otf)$/,
          use: [
            'file-loader'
          ]
        },
        {
          test: /\.(csv|tsv)$/,
          use: [
            'csv-loader'
          ]
        },
        {
          test: /\.xml$/,
          use: [
            'xml-loader'
          ]
        },
        {
          test: /\.js$/,
          use: ['source-map-loader'],
          enforce: 'pre',
        }
     ]
   }
  };