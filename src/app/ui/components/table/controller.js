import Format from '../../../no-ui/format';

class Controller {
  constructor() {
    this.dataTable = [
      {
        name: 'Name1',
        account_name: 'Account Name 1',
        birthday: new Date(),
        salary: 1835.20,
        currency: 'EUR'
      },
      {
        name: 'Name2',
        account_name: 'Account Name 2',
        birthday: new Date(),
        salary: 235.20,
        currency: 'EUR'
      },
      {
        name: 'Name3',
        account_name: 'Account Name 3',
        birthday: new Date(),
        salary: 1835.24,
        currency: 'EUR'
      },
      {
        name: 'Name4',
        account_name: 'Account Name 4',
        birthday: new Date(),
        salary: 1125.230,
        currency: 'EUR'
      },
      {
        name: 'Name5',
        account_name: 'Account Name 5',
        birthday: new Date(),
        salary: 135.2680,
        currency: 'EUR'
      },
    ];
  }

  formatingDate(value) {
    return  Format.date(value) 
  }

  formatingCurrency(value) {
    return Format.currency(value);
  }

  show(item) {
    this.showModal({ contact: item });
  }
}

export default Controller;