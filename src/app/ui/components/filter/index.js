import angular from 'angular';

import template from './template.html';
import controller from './controller';

import './styles.scss';

const config = {
  bindings: {
    hide: '&',
    save: '&'
  },
  template,
  controller,
};

export default angular.module('ui.components.filter', [])
  .component('kpFilter', config);
