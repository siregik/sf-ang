class Controller {
  constructor() {
    this.filter = {};
  }


  $onInit() {
  }

  closeFilter() {
    this.hide();
  }

  reset() {
    this.filter = {};
  }
}

export default Controller;