class Controller {
  constructor() {
    
  }

  $onInit() {
  }

  closeModal() {
    this.hide();
  }

  saveState() {
    this.save();
  }
}

export default Controller;