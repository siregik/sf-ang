import angular from 'angular';

import template from './template.html';
import controller from './controller';

import './styles.scss';

const config = {
  bindings: {
    title: '<',
    hide: '&',
    save: '&',
  },
  template,
  controller,
  transclude: true,
};

export default angular.module('ui.components.modal', [])
  .component('kpModal', config);
