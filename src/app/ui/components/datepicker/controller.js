import Format from '../../../no-ui/format';

class Controller {
  constructor() {
    this.date = Format.date(this.model);
  }
}

export default Controller;