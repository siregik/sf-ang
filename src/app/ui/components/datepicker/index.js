import angular from 'angular';
import 'angularjs-datepicker';

import template from './template.html';
import controller from './controller';

import './angular-datepicker.css'
import './styles.scss';

const config = {
  bindings: {
    model: '='
  },
  template,
  controller,
};

export default angular.module('ui.components.datepicker', [
  '720kb.datepicker',
])
  .component('kpDatepicker', config);
