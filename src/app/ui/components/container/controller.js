class Controller {
  constructor() {
    this.title = 'Component container';
    this.showModalContact = false;
    this.showFilterTable = false;
    this.contact = {};
    this.titleModal = 'Create new contact';
  }

  $onInit() {}

  showModal(item) {
    this.contact = item || {};
    this.showModalContact = true;
  }

  hideModal() {
    this.showModalContact = false;
  }

  showFilter() {
    this.showFilterTable = true;
  }

  hideFilter() {
    this.showFilterTable = false;
  }
}

export default Controller;