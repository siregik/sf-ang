import angular from 'angular';

import template from './template.html';
import controller from './controller';

import './styles.scss';

const config = {
  bindings: {
    myCorpModels: '<'
  },
  template,
  controller,
  transclude: true,
};

export default angular.module('ui.components.container', [])
  .component('kpContainer', config);
