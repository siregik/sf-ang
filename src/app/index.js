import angular from 'angular';

import KpTable from './ui/components/table'
import KpContainer from './ui/components/container'
import KpDatepicker from './ui/components/datepicker'
import KpModal from './ui/components/modal'
import KpFormContact from './ui/components/forms/form-contact'
import KpFilter from './ui/components/filter'
import AppController from './AppController';

import '../assets/styles.scss'
// @TODO: need to fix switching salesforce-lightning-design-system to the project
import '../../node_modules/@salesforce-ux/design-system/assets/styles/salesforce-lightning-design-system.css'

export default angular.module('app', [
  KpTable.name,
  KpContainer.name,
  KpDatepicker.name,
  KpModal.name,
  KpFormContact.name,
  KpFilter.name
])
.controller('AppController', AppController)
.run();