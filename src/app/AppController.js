/**
 * Main App Controller
 * @constructor
 */
class AppController {
  constructor(){
    this.title = 'Title AppController';
  }
}

export default AppController;
