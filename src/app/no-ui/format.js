import moment from 'moment';
import n from 'numeral';

class Format {
  static date(value) {
    return moment(value).format('MM/DD/YYYY');
  }
  
  static currency(value) {
    return n(value).format('0,0[.][00]');
  }
}

export default Format;